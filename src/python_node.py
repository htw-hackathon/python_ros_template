#!/usr/bin/env python

# Sven Eckelmann eckelmann@htw-dresden.de
# Date: 03/08/201
# Revision 2

import rospy
from ego.msg import EgoMsg

def callback(data):
    rospy.loginfo(data.velocity)



def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.

    rospy.Subscriber('/egodata', EgoMsg, callback)

    rospy.spin()


def main():
    rospy.init_node('python_example', anonymous = False)

    # Case def for choosen vehicle
    rospy.loginfo('python example initiated')

    listener()



if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
